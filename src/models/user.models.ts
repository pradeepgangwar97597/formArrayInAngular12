export class UserCheck {
    status: boolean;
    response: number;
    message: string;
    twostep: number;
    authToken: string;
}

export class User {
    status: boolean;
    response: number;
    message: string;
    userid: number;
    username: string;
    name: string;
    email: string;
    phone : string;
    address : string;
    usertype : string;
    validToken: number;
}
