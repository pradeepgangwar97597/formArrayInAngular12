import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User, UserCheck } from 'src/models/user.models';
import { tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiServiceService {
  public apiToken: string = 'e090c25187ee2b3f9f1f8a02747356641';

  constructor(private http: HttpClient) {
  }

  public loginUser(userRequest): Observable<UserCheck> {
    return this.http.post<UserCheck>('https://rnfi.co.in/latest-backup/api/app/task/login/login', userRequest).pipe(tap(data => { }));
  }
  public verificationUser(otpReq): Observable<User> {
    return this.http.post<User>('https://rnfi.co.in/latest-backup/api/app/task/login/verifyOtp', otpReq).pipe(tap(data => { }));
  }
  public getListData(req): Observable<{}> {
    return this.http.post<{}>('https://paysprint.in/service-api/testangular/api/TestAngular/getDynamicform', req, { headers: { Authkey: 'test-angular-2021' } });
  }
  public updateListData(req): Observable<{}> {
    return this.http.post<{}>('https://paysprint.in/service-api/testangular/api/TestAngular/createDynamicform', req, { headers: { Authkey: 'test-angular-2021' } });
  }
}
