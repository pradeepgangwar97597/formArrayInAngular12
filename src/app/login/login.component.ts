import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NavigationExtras, Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  loginForm: FormGroup;
  submitted: boolean;
  message: string;
  isLoading: boolean;

  constructor(private fb: FormBuilder, public router: Router, public service: ApiServiceService) { }

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      username: new FormControl('', Validators.compose([Validators.required])),
      password: ['', [Validators.required, Validators.minLength(6)]],
    })
  }
  onSubmitForm() {
    this.submitted = true;
    if (this.loginForm.invalid) {
      return;
    }
    let fomrData = new FormData();
    fomrData.append('username', this.loginForm.value.username);
    fomrData.append('password', this.loginForm.value.password);
    fomrData.append('token', this.service.apiToken);
    this.isLoading = true;
    this.message = "Loading..";
    this.service.loginUser(fomrData).subscribe((res) => {
      this.message = res.message;
      this.isLoading = false;
      if (res.twostep == 1) {
        let navigationExtras: NavigationExtras = { queryParams: { authToken: res.authToken } };
        this.router.navigate(['./verification'], navigationExtras);
      } else {
        // this.router.navigate(['/home']);
      }
    }, err => {
      this.isLoading = false;
      this.message = 'User not found..!!';
    })
  }
}