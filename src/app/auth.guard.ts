import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(public router: Router) { }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    if (JSON.parse(localStorage.getItem('userData')) != null && window.localStorage.getItem('authToken') != null) {
      if (route.routeConfig.path == "login" || route.routeConfig.path == "verification") {
        return this.router.navigate(['home']);
      } else {
        return true;
      }
    } else {
      if (route.routeConfig.path == "login" || route.routeConfig.path == "verification") {
        return true;
      } else {
        return this.router.navigate(['login']);
      }
    }
  }
}
