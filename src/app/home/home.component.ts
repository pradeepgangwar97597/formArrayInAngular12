import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/models/user.models';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  isLoading: boolean;
  message: string;
  user: User;
  listArray = new Array<any>();
  listForm: FormGroup;

  constructor(private formbuilder: FormBuilder, public router: Router, public service: ApiServiceService) {
  }

  ngOnInit(): void {
    this.user = JSON.parse(localStorage.getItem('userData'))
    this.getListData();
    this.listGroup();
  }

  updateGroup() {
    this.listGroup(true);
    this.listArray.map((group, index) => {
      const control1 = <FormArray>this.listForm.controls['arrayList']; control1.push(this.groupForm(group));
      group.array.map((res, index1) => {
        const control2 = <FormArray>this.listForm.get('arrayList')['controls'][index].get('list');
        control2.push(this.arrayForm(res));
        res.map(final => {
          const control3 = <FormArray>this.listForm.get('arrayList')['controls'][index].get('list')['controls'][index1].get('listing');
          control3.push(this.final(final));
        })
      })
    })
  }

  addGroup() {
    if (this.listForm.controls.arrayList['controls'].length < 5) {
      const control = <FormArray>this.listForm.controls['arrayList'];
      control.push(this.groupForm());
    }
  }

  listGroup(update?: boolean) {
    this.listForm = this.formbuilder.group({ arrayList: this.formbuilder.array(update ? [] : [this.groupForm()]) });
  }

  groupForm(group?) {
    return this.formbuilder.group({
      title: [group ? group.key : ''],
      list: this.formbuilder.array((group ? [] : [this.arrayForm()]))
    });
  }

  arrayForm(group?) {
    return this.formbuilder.group({
      listing: this.formbuilder.array((group ? [] : [this.final()]))
    })
  }

  final(choice?) {
    return this.formbuilder.group({
      name: [choice ? choice[0] : '', Validators.required],
      value: [choice ? choice[1] : '', [Validators.required]],
    })
  }

  getListData() {
    let fomrData = new FormData();
    fomrData.append('token', this.service.apiToken);
    fomrData.append('authToken', window.localStorage.getItem('authToken'));
    this.isLoading = true;
    this.message = "Loading..";
    this.service.getListData(fomrData).subscribe((res: any) => {
      this.message = null;
      this.isLoading = false;
      var finalArray = []
      let list = res.data.map(res => Object.entries(res))[0];
      list.map(res => {
        var objArray = res[1].map(res1 => {
          return res1 = Object.entries(res1);
        })
        finalArray.push({ key: res[0], array: objArray })
      })
      this.listArray = finalArray
      if (this.listArray) this.updateGroup()

    }, err => {
      this.isLoading = false;
      this.message = 'Something went wrong';
    })
  }

  updateList() {
    let fomrData = new FormData();
    fomrData.append('token', this.service.apiToken);
    fomrData.append('authToken', window.localStorage.getItem('authToken'));
    fomrData.append("json",JSON.stringify(this.listForm.value.arrayList))
    this.isLoading = true;
    this.message = "Loading..";
    this.service.updateListData(fomrData).subscribe((res: any) => {
      // this.message = null;
      // this.isLoading = false;
    }, err => {
      this.isLoading = false;
      this.message = 'Something went wrong';
    })
  }
}
