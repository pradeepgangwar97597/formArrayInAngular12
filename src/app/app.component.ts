import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { User } from 'src/models/user.models';
import { ApiServiceService } from './api-service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'rnfiTest';
  logedIn: string;
  user: User;

  constructor(public router: Router, private service: ApiServiceService) {
    this.user = JSON.parse(localStorage.getItem('userData'))
    router.events.subscribe((val) => {
      console.log("call")
      this.user = JSON.parse(localStorage.getItem('userData'))
    });

  }
  navigateFun() {
    if (this.user) {
      this.router.navigate(['/home'])
    } else {
      this.router.navigate(['/login'])
    }
  }
  logout() {
    window.localStorage.removeItem("userData")
    this.router.navigate(['./login']);
  }
}
