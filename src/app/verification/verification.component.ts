import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiServiceService } from '../api-service.service';

@Component({
  selector: 'app-verification',
  templateUrl: './verification.component.html',
  styleUrls: ['./verification.component.scss']
})
export class VerificationComponent implements OnInit {
  otpForm: FormGroup;
  authToken: string;
  submitted: boolean;
  message: string;
  isLoading: boolean;

  constructor(private fb: FormBuilder, private route: ActivatedRoute, public router: Router, public service: ApiServiceService) {
    this.route.queryParams.subscribe(params => {
      this.authToken = params["authToken"];
    });
  }

  ngOnInit(): void {
    this.otpForm = this.fb.group({
      otp: new FormControl('', Validators.compose([Validators.required])),
    })
  }

  onSubmitForm() {
    this.submitted = true;
    if (this.otpForm.invalid) {
      return;
    }
    let fomrData = new FormData();
    fomrData.append('otp', this.otpForm.value.otp);
    fomrData.append('token', this.service.apiToken);
    fomrData.append('authToken', this.authToken);
    this.isLoading = true;
    this.message = "Loading..";
    this.service.verificationUser(fomrData).subscribe((res) => {
      this.message = res.message;
      this.isLoading = false;
      window.localStorage.setItem('authToken', this.authToken);
      window.localStorage.setItem('userData', JSON.stringify(res));
      this.router.navigate(['/home']);
    }, err => {
      this.isLoading = false;
      this.message = 'Invalid otp..!!';
    })
  }
}
